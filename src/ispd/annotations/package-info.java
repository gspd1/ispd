/**
 * A package for custom {@link java.lang.annotation annotations} used by the application.
 * <p>
 * This package contains the following classes:
 * <ul>
 *     <li>{@link ispd.annotations.ConcretePolicy ConcretePolicy}: an annotation to be used in
 *     concrete subclasses of the {@link ispd.policy.Policy Policy} abstract class.</li>
 * </ul>
 */
package ispd.annotations;
