package ispd.policy;

/**
 * An enumeration of possible conditions under which a policy may be executed.
 */
public enum PolicyCondition {
    /**
     * The policy should be executed whenever the corresponding master receives a result.
     */
    WHEN_RECEIVES_RESULT,
    /**
     * The policy should be executed while there are still items (tasks, virtual machines) that must
     * be distributed.
     */
    WHILE_MUST_DISTRIBUTE,
}
