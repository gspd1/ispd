package ispd.policy.managers.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Utility class for extracting directories from a JAR file.
 * <p>
 * This class provides a method, {@link #extractDirsFromJar()}, which extracts two directories from
 * the JAR file specified by the {@literal java.class.path} system property:
 * <ul>
 *     <li>one is specified by the {@link #targetDirectory} field, which is initialized from a
 *     constructor argument.</li>
 *     <li>the other has the fixed name {@value #MOTOR_PKG_DIRECTORY_PATH}</li>
 * </ul>
 * <p>
 * The directories are extracted by filtering the entries in the JAR file and writing its
 * contents with matching entries to the filesystem. Containing directories are created as
 * necessary.
 */
public class JarExtractionUtility {
    private static final String JAR_PREFIX = "jar:";
    private static final String MOTOR_PKG_DIRECTORY_PATH = "motor";
    private final ZipFile jar;
    private final String targetDirectory;

    /**
     * Constructs a new instance, which will extract the given {@link #targetDirectory}, along with
     * the {@value #MOTOR_PKG_DIRECTORY_PATH} package.
     *
     * @param targetDirectory the name of the target directory to extract from the JAR file.
     *
     * @throws IOException if an error occurs while instantiating a {@link JarFile} from the
     *                     {@literal java.class.path} system property.
     */
    public JarExtractionUtility(final String targetDirectory) throws IOException {
        this.targetDirectory = targetDirectory;
        this.jar = new JarFile(new File(System.getProperty("java.class.path")));
    }

    /**
     * Returns whether the current execution is being run from a jar file.
     *
     * @return {@code true} if the current execution is being run from a jar file, {@code false}
     * otherwise.
     */
    public static boolean isApplicationExecutingFromJar() {
        final var url = JarExtractionUtility.class
                .getResource(JarExtractionUtility.class.getCanonicalName());
        final var executableName = Objects.requireNonNull(url).toString();
        return executableName.startsWith(JarExtractionUtility.JAR_PREFIX);
    }

    /**
     * Extracts two directories from the JAR file pointed to by the {@literal java.class.path}
     * system property: one specified by the {@link #targetDirectory} field, and another with a
     * fixed name of {@value #MOTOR_PKG_DIRECTORY_PATH}.
     *
     * @throws IOException if an error occurs while reading from the JAR file or creating/writing
     *                     the needed files in the filesystem.
     */
    public void extractDirsFromJar() throws IOException {
        this.extractDirFromJar(this.targetDirectory);
        this.extractDirFromJar(JarExtractionUtility.MOTOR_PKG_DIRECTORY_PATH);
    }

    private void extractDirFromJar(final String dir) throws IOException {
        final var entries = this.jar.stream()
                .filter(e -> e.getName().contains(dir))
                .toList();

        for (final var entry : entries) {
            this.processZipEntry(entry);
        }
    }

    private void processZipEntry(final ZipEntry entry) throws IOException {
        final var file = new File(entry.getName());

        if (entry.isDirectory() && !file.exists()) {
            Files.createDirectory(file.toPath());
            return;
        }

        final var parent = file.getParentFile();

        if (!parent.exists()) {
            Files.createDirectory(parent.toPath());
        }

        try (final var is = this.jar.getInputStream(entry);
             final var os = new FileOutputStream(file)) {
            is.transferTo(os);
        }
    }
}
