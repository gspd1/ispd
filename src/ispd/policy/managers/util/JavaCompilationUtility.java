package ispd.policy.managers.util;

import javax.tools.Tool;
import javax.tools.ToolProvider;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Utility class for compiling Java code.
 * <p>
 * Each new instance created is responsible for compiling a specific target file.
 * <p>
 * The {@code JavaCompilationUtility} class provides a {@link #compile()} method that can be used to
 * compile a target Java file.
 * <p>
 * If the system's Java compiler is available (via the {@link ToolProvider} utility), it will be
 * used to compile the file. Otherwise, this class will fall back into using the {@literal javac}
 * command in the system's command line interface (via the {@link Runtime} class).
 */
public class JavaCompilationUtility {
    private final Optional<Tool> systemJavaCompiler =
            Optional.ofNullable(ToolProvider.getSystemJavaCompiler());
    private final File target;

    /**
     * Constructs a new {@code JavaCompilationUtility} instance, responsible for compiling the given
     * target {@link File}.
     *
     * @param target the target Java file to compile.
     */
    public JavaCompilationUtility(final File target) {
        this.target = target;
    }

    /**
     * Attempts to compile the target Java file given in the constructor. It returns a
     * {@link String} containing hte error output produced during the compilation process. In the
     * case of a successful compilation, the returned {@link String} will be empty.
     *
     * @return a {@link String} containing the error output produced in the compilation process. It
     * will be empty in the case of a successful compilation.
     */
    public String compile() {
        return this.systemJavaCompiler
                .map(this::attemptCompilationWithSystemJavaCompiler)
                .orElseGet(this::attemptCompilationWithJavac);
    }

    private String attemptCompilationWithSystemJavaCompiler(final Tool tool) {
        final var err = new ByteArrayOutputStream();
        final var arg = this.target.getPath();
        tool.run(null, null, err, arg);
        return err.toString();
    }

    private String attemptCompilationWithJavac() {
        try {
            return this.runCompilationWithJavac();
        } catch (final IOException ex) {
            return "Compilation via Javac failed!";
        }
    }

    private String runCompilationWithJavac() throws IOException {
        final var command = "javac %s".formatted(this.target.getPath());
        final var process = Runtime.getRuntime().exec(command);

        try (final var err = new BufferedReader(new InputStreamReader(
                process.getErrorStream(), StandardCharsets.UTF_8
        ))) {
            return err.lines().collect(Collectors.joining("\n"));
        }
    }
}
