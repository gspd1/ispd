package ispd.policy.managers;

import ispd.arquivo.xml.ConfiguracaoISPD;
import ispd.motor.filas.servidores.CentroServico;
import ispd.policy.PolicyManager;
import ispd.policy.vmallocation.VmAllocationPolicy;

import java.io.File;
import java.util.List;

/**
 * This class is a specialization of the {@link FilePolicyManager} class for <i>VM allocation</i>
 * policies.
 * <p>
 * This class also defines a list of native VM allocation policies available by default, which can
 * be accessed via the {@link #NATIVE_POLICIES} static field.
 *
 * @see FilePolicyManager The base class that this class extends
 * @see PolicyManager The interface that this class and its base class implement
 */
public class VmAllocationPolicyManager extends FilePolicyManager {
    /**
     * A list of native VM allocation policies available to the user by default.
     * <p>
     * The list includes the following policies' names:
     * <ul>
     *     <li>{@link ispd.policy.vmallocation.impl.RoundRobin RoundRobin}</li>
     *     <li>{@link ispd.policy.vmallocation.impl.FirstFit FirstFit}</li>
     *     <li>{@link ispd.policy.vmallocation.impl.FirstFitDecreasing FirstFitDecreasing}</li>
     *     <li>{@link ispd.policy.vmallocation.impl.Volume Volume}</li>
     * </ul>
     * <p>
     * It also contains the {@link #NO_POLICY} constant ({@value #NO_POLICY}), which represents
     * the absence of a policy, for convenience of selection menus that wish to use this list.
     */
    public static final List<String> NATIVE_POLICIES = List.of(
            PolicyManager.NO_POLICY,
            "RoundRobin", "FirstFit", "FirstFitDecreasing", "Volume"
    );

    /**
     * The path to the directory where VM allocation policies are stored on the filesystem, relative
     * to the ISPD root directory (as specified by the {@link ConfiguracaoISPD#DIRETORIO_ISPD}
     * field).
     * <p>
     * The full path is: {@code [DiretorioISPD]/policies/vmallocation/}
     */
    private static final String VM_DIR_PATH =
            String.join(File.separator, "policies", "vmallocation");

    /**
     * The {@link File} instance corresponding to the directory where VM allocation policies are
     * stored on the filesystem.
     * <p>
     * The directory is located under the iSPD root directory (as specified by the
     * {@link ConfiguracaoISPD#DIRETORIO_ISPD} field).
     * <p>
     * The directory's full path is: {@code [DiretorioISPD]/policies/vmallocation/}
     */
    private static final File VM_DIRECTORY =
            new File(ConfiguracaoISPD.DIRETORIO_ISPD, VmAllocationPolicyManager.VM_DIR_PATH);
    private static final String VM_PKG = "alocacaoVM";

    /**
     * The {@link File} instance corresponding to the directory where VM allocation policies are
     * stored on the filesystem.
     * <p>
     * The directory is as defined in the {@link #VM_DIRECTORY} static field.
     * <p>
     * The directory's full path is: {@code [DiretorioISPD]/policies/vmallocation/}
     *
     * @return the directory where VM allocation policies are stored on the filesystem.
     */
    @Override
    public File getPolicyDirectory() {
        return VmAllocationPolicyManager.VM_DIRECTORY;
    }

    /**
     * Returns the name of the package where VM allocation policies are stored.
     * <p>
     * The package name is {@value #VM_PKG}.
     *
     * @return the name of the package where VM allocation policies are stored.
     */
    @Override
    protected String getPolicyPackageName() {
        return VmAllocationPolicyManager.VM_PKG;
    }

    /**
     * Returns the source code 'raw' template for VM allocation policies.
     * <hr>
     * <p>
     * The template includes a class that extends the abstract
     * {@link VmAllocationPolicy VmAllocationPolicy}, overriding its abstract methods with
     * implementations that merely throw an {@link UnsupportedOperationException}.
     * <p>
     * These implementations must be substituted by the user in order to create a functional
     * scheduling policy. The overridden methods that must be properly implemented are:
     * <ul>
     *     <li>{@link VmAllocationPolicy#initialize() initialize}</li>
     *     <li>{@link VmAllocationPolicy#schedule() schedule}</li>
     *     <li>{@link VmAllocationPolicy#scheduleRoute(CentroServico) scheduleRoute}</li>
     *     <li>{@link VmAllocationPolicy#scheduleResource() scheduleResource}</li>
     *     <li>{@link VmAllocationPolicy#scheduleVirtualMachine() scheduleVirtualMachine}</li>
     * </ul>
     * <p>
     * The class name is left as {@value FilePolicyManager#POLICY_NAME_REPL}, to be replaced
     * during runtime, as specified by {@link FilePolicyManager#getPolicyTemplate(String)}.
     *
     * @return the source code template for VM allocation policies.
     *
     * @see FilePolicyManager#POLICY_NAME_REPL
     * @see FilePolicyManager#getPolicyTemplate(String)
     * @see VmAllocationPolicy
     * @see ispd.policy.Policy Policy
     */
    @Override
    protected String getRawPolicyTemplate() {
        //language=JAVA
        return """
                package ispd.policy.externo;
                                
                import ispd.motor.filas.servidores.CS_Processamento;
                import ispd.motor.filas.servidores.CentroServico;
                import ispd.motor.filas.servidores.implementacao.CS_VirtualMac;
                import ispd.policy.vmallocation.VmAllocationPolicy;
                                
                import java.util.List;
                                
                public class __POLICY_NAME__ extends VmAllocationPolicy {
                    @Override
                    public void initialize() {
                        throw new UnsupportedOperationException("Not Implemented Yet.");
                    }
                           
                    @Override
                    public void schedule() {
                        throw new UnsupportedOperationException("Not Implemented Yet.");
                    }
                                
                    @Override
                    public List<CentroServico> scheduleRoute(final CentroServico destination) {
                        throw new UnsupportedOperationException("Not Implemented Yet.");
                    }
                                
                    @Override
                    public CS_Processamento scheduleResource() {
                        throw new UnsupportedOperationException("Not Implemented Yet.");
                    }
                                
                    @Override
                    public CS_VirtualMac scheduleVirtualMachine() {
                        throw new UnsupportedOperationException("Not Implemented Yet.");
                    }
                }
                               """;
    }
}
