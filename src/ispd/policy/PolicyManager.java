package ispd.policy;

import java.io.File;
import java.util.List;

/**
 * An interface for managing scheduling and allocation policies.
 * <p>
 * The {@code PolicyManager} interface provides method signatures for listing, writing, compiling,
 * reading and importing policies. It also contains a method that returns a
 * {@link #getPolicyTemplate(String)} template} for writing a new policy's source code.
 * <p>
 * The {@link #NO_POLICY} static field is a constant that represents the absence of a policy.
 *
 * @see ispd.policy.managers.FilePolicyManager Abstract policy manager implementation
 * @see ispd.policy.managers.GridSchedulingPolicyManager Concrete grid scheduling policy manager
 * implementation
 * @see ispd.policy.managers.CloudSchedulingPolicyManager Concrete cloud scheduling policy manager
 * implementation
 * @see ispd.policy.managers.VmAllocationPolicyManager Concrete VM allocation policy manager
 * implementation
 */
public interface PolicyManager {
    /**
     * A constant with value {@value}, used to represent the absence of a policy.
     * <p>
     * May be used in list selections or other contexts were using an empty {@link String} to
     * represent the absence of a policy may cause problems.
     */
    String NO_POLICY = "---";

    /**
     * Gets the directory, as a {@code File} object, in which managed policies are stored.
     *
     * @return the directory where managed policies are stored.
     */
    File getPolicyDirectory();

    /**
     * Lists the currently available policies, which does not include policies removed during this
     * instance's lifetime. To list removed policies, use method {@link #listRemovedPolicies()}.
     *
     * @return a list of the names of the available policies.
     */
    List<String> listAvailablePolicies();

    /**
     * Lists the policies that have been successfully added during this instance's lifetime.
     *
     * @return a list of the policies that have been added.
     */
    List<String> listAddedPolicies();

    /**
     * Lists the policies that have been successfully removed during this instance's lifetime.
     *
     * @return a list of the policies that have been removed.
     */
    List<String> listRemovedPolicies();

    /**
     * Reads the source code of the policy with the given name. The policy must have been previously
     * saved via the {@link #savePolicy(String, String) save} method.
     *
     * @param policyName the name of the policy.
     *
     * @return the source code for the policy, or {@code null} if an error occurs during reading.
     */
    String getPolicyCode(String policyName);

    /**
     * Saves the policy with given name and source code to a file.
     * <p>
     * The file's path is decided internally, via a combination of the policy's name and the
     * directory given via the {@link #getPolicyDirectory()} method.
     *
     * @param policyName the name of the policy.
     * @param policyCode the source code for the policy (i.e., contents of the saved file).
     */
    void savePolicy(String policyName, String policyCode);

    /**
     * Compiles the policy with the given name.
     *
     * @param policyName the name of the policy to be compiled.
     *
     * @return a {@link String} containing errors found during compilation, or {@code null} if no
     * errors occurred.
     */
    String compilePolicy(String policyName);

    /**
     * Attempts to import a policy whose source code is in the given file. The policy name is, by
     * default, the name of the given file without the {@literal .java} suffix.
     * <p>
     * The method copies the file contents to a new file in the standard policy directory (given by
     * the method {@link #getPolicyDirectory()}). Then, it attempts to compile the new policy.
     *
     * @param file the file containing source code for the new policy, and whose name will be used
     *             as the new policy's name.
     *
     * @return {@code true} if the policy was successfully imported, {@code false} otherwise.
     */
    boolean importPolicy(File file);

    /**
     * Attempts to remove the policy with the given name from the saved policies.
     *
     * @param policyName the name of the policy to be removed.
     *
     * @return {@code true} if the policy was successfully removed, {@code false} otherwise.
     */
    boolean removePolicy(String policyName);

    /**
     * Gets a source code template, with the given policy name injected, for writing a new policy
     * with the given name.
     *
     * @param policyName the name of the policy to be inserted into the template.
     *
     * @return the template for writing the code for a policy with the given name.
     */
    String getPolicyTemplate(String policyName);
}
