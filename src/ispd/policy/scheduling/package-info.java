/**
 * The {@code scheduling} package contains:
 * <ul>
 *     <li>{@link ispd.policy.scheduling.SchedulingMaster SchedulingMaster}: A scheduling
 *     specialization of {@link ispd.policy.PolicyMaster PolicyMaster}.</li>
 *     <li>{@link ispd.policy.scheduling.SchedulingPolicy SchedulingPolicy}: A scheduling
 *     specialization of {@link ispd.policy.Policy Policy}.</li>
 *     <li>Two inner sub-packages, {@link ispd.policy.scheduling.grid grid} and
 *     {@link ispd.policy.scheduling.cloud cloud}, that hold specializations and implementations
 *     for grid and cloud-based scheduling policies, respectively.</li>
 * </ul>
 */
package ispd.policy.scheduling;