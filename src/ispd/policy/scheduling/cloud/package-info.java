/**
 * The {@code cloud} package contains:
 * <ul>
 *     <li>{@link ispd.policy.scheduling.cloud.CloudMaster GridMaster}: A specialization
 *     of {@link ispd.policy.PolicyMaster PolicyMaster} for cloud-based scheduling.</li>
 *     <li>{@link ispd.policy.scheduling.cloud.CloudSchedulingPolicy GridSchedulingPolicy}: A
 *     specialization of {@link ispd.policy.scheduling.SchedulingPolicy SchedulingPolicy} for
 *     cloud-based scheduling.</li>
 *     <li>The subpackage {@link ispd.policy.scheduling.cloud.impl impl}, which holds
 *     implementations for concrete cloud-based scheduling policies.</li>
 * </ul>
 */
package ispd.policy.scheduling.cloud;