package ispd.policy.scheduling.cloud;

import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.implementacao.CS_VirtualMac;
import ispd.policy.Policy;
import ispd.policy.scheduling.SchedulingPolicy;
import ispd.policy.scheduling.cloud.impl.RoundRobin;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The {@code CloudSchedulingPolicy} class is an abstract specialization of {@link SchedulingPolicy}
 * for the representation of task scheduling algorithms in cloud computing systems.
 * <p>
 * It specializes the parameterization of {@link SchedulingPolicy} with the {@link CloudMaster}
 * interface. It adds a single new method to the interface: {@link #getAdequateVirtualMachinesFor},
 * which returns a list of {@link CS_Processamento} that meet certain criteria.
 * <p>
 * This class does not have further specializations to differentiate policy types, only concrete
 * implementations. They can be found in the {@link ispd.policy.scheduling.cloud.impl impl}
 * subpackage.
 *
 * @see SchedulingPolicy The superclass this one specializes
 * @see CloudMaster The PolicyMaster specialization this class is associated with
 */
public abstract class CloudSchedulingPolicy extends SchedulingPolicy<CloudMaster> {
    /**
     * Returns a list of {@link CS_Processamento} instances that are adequate for the user with the
     * given ID.
     * <p>
     * Adequacy is defined as:
     * <ol>
     *     <li>Being owned by the desired user; that is, the machine's
     *     {@link CS_Processamento#getProprietario() getProprietario} should return the same user
     *     ID as the given one.</li>
     *     <li>Being in the {@link CS_VirtualMac#ALOCADA ALOCADA} state; that is, the machine's
     *     {@link CS_VirtualMac#getStatus() getStatus} should return this constant.</li>
     * </ol>
     * <p>
     * This method is implemented as a stream operation over the field {@link Policy#slaves slaves},
     * filtering for {@link CS_VirtualMac} instances that meet the criteria described above. They
     * are then collected into a {@link LinkedList}.
     *
     * @param userId the Id of the user for whom the returned {@link CS_Processamento} instances
     *               should be adequate.
     *
     * @return a list of {@link CS_Processamento} instances that are adequate for the given user
     *
     * @throws ClassCastException if one of the {@link CS_Processamento} objects in the
     *                            {@link #slaves} field owned by the given user is not a
     *                            {@link CS_VirtualMac} instance.
     * @see RoundRobin#schedule() Where this method is used.
     */
    protected List<CS_Processamento> getAdequateVirtualMachinesFor(final String userId) {
        return this.slaves.stream()
                .filter(s -> s.getProprietario().equals(userId))
                .map(CS_VirtualMac.class::cast)
                .filter(s -> s.getStatus() == CS_VirtualMac.ALOCADA)
                .collect(Collectors.toCollection(LinkedList::new));
    }
}
