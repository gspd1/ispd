package ispd.policy.scheduling.cloud;

import ispd.policy.scheduling.SchedulingMaster;

/**
 * The {@code CloudMaster} interface represents a class that can execute scheduling operations in a
 * cloud-based computing system by communicating with the currently unfolding
 * {@link ispd.motor.Simulation Simulation}. Besides its {@link SchedulingMaster} capabilities, it
 * can also forcefully permit a scheduling cycle.
 * <p>
 * It extends the {@link SchedulingMaster} interface and adds a single method specific to
 * cloud-based scheduling policies, {@link #makeSchedulingAvailable()}, which forces this master to
 * consider task scheduling requirements as met.
 * <p>
 * This class has no further specializations. An example of a concrete implementation of this
 * interface is {@link ispd.motor.filas.servidores.implementacao.CS_VMM CS_VMM}.
 * <p>
 * This type of master is associated with the {@link CloudSchedulingPolicy} policy type.
 *
 * @see SchedulingMaster The base interface for classes that can call methods on scheduling policies
 * @see CloudSchedulingPolicy Policy type that uses this master type.
 * @see ispd.motor.filas.servidores.implementacao.CS_VMM Concrete implementation of the interface
 */
public interface CloudMaster extends SchedulingMaster {
    /**
     * Force this master to consider task scheduling requirements as met.
     * <p>
     * The scheduling cycle may still be impeded by VM allocation requirements.
     *
     * @see ispd.policy.scheduling.cloud.impl.RoundRobin Where this method is used.
     */
    void makeSchedulingAvailable();
}
