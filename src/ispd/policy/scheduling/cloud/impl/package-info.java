/**
 * This package contains the following concrete implementations of cloud-based scheduling policies:
 * <ul>
 *     <li>{@link ispd.policy.scheduling.cloud.impl.RoundRobin RoundRobin}: A simple scheduling
 *     algorithm. Schedules tasks in a FIFO manner and resources in a Round-Robin fashion.</li>
 * </ul>
 */
package ispd.policy.scheduling.cloud.impl;