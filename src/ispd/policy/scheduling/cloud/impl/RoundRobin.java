package ispd.policy.scheduling.cloud.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.filas.Tarefa;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.policy.scheduling.cloud.CloudSchedulingPolicy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Implementation of the RoundRobin scheduling algorithm. Hands over the next task on the FIFO
 * queue, for the next resource in a circular queue of resources.
 */
@ConcretePolicy
public class RoundRobin extends CloudSchedulingPolicy {
    private ListIterator<CS_Processamento> resources = null;
    private LinkedList<CS_Processamento> slavesUser = null;

    public RoundRobin() {
        this.tasks = new ArrayList<>(0);
        this.slaves = new LinkedList<>();
    }

    @Override
    public void initialize() {
        this.slavesUser = new LinkedList<>();
        this.resources = this.slavesUser.listIterator(0);
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destino) {
        final var destination = (CS_Processamento) destino;
        final int index = this.slaves.indexOf(destination);
        return new ArrayList<>((List<CentroServico>) this.slaveRoutes.get(index));
    }

    @Override
    public void schedule() {
        final var task = this.scheduleTask();
        final var taskOwner = task.getProprietario();
        this.slavesUser = (LinkedList<CS_Processamento>)
                this.getAdequateVirtualMachinesFor(taskOwner);

        if (this.slavesUser.isEmpty()) {
            this.noAllocatedVms(task);
        } else {
            this.scheduleTask(task);
        }
    }

    @Override
    public Tarefa scheduleTask() {
        return this.tasks.remove(0);
    }

    @Override
    public CS_Processamento scheduleResource() {
        if (!this.resources.hasNext()) {
            this.resources = this.slavesUser.listIterator(0);
        }
        return this.resources.next();
    }

    private void noAllocatedVms(final Tarefa task) {
        this.submitTask(task);
        this.policyMaster.makeSchedulingAvailable();
    }

    private void scheduleTask(final Tarefa task) {
        final var resource = this.scheduleResource();
        task.setLocalProcessamento(resource);
        task.setCaminho(this.scheduleRoute(resource));
        this.policyMaster.sendTask(task);
    }
}