package ispd.policy.scheduling;

import ispd.motor.FutureEvent;
import ispd.motor.Mensagens;
import ispd.motor.filas.Tarefa;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.policy.PolicyCondition;
import ispd.policy.PolicyConditionSets;
import ispd.policy.PolicyMaster;

import java.util.Set;

/**
 * The {@code SchedulingMaster} interface represents a class that can execute scheduling operations
 * by communicating with the currently unfolding {@link ispd.motor.Simulation Simulation}. It can
 * manage tasks and messages, and update its subordinates.
 * <p>
 * It extends the {@link PolicyMaster} interface and adds several methods specific to scheduling
 * policies, including a method to update the {@link PolicyCondition}s under which it may execute.
 *
 * @see PolicyMaster The base interface for classes that can call methods on scheduling or
 * allocation policies
 * @see SchedulingPolicy Policy type that uses this master type.
 * @see ispd.policy.scheduling.grid.GridMaster Grid's specialization of the interface
 * @see ispd.policy.scheduling.cloud.CloudMaster Cloud's specialization of the interface
 * @see ispd.motor.filas.servidores.implementacao.CS_Mestre Grid's concrete implementation of the
 * interface
 * @see ispd.motor.filas.servidores.implementacao.CS_VMM Cloud's concrete implementation of the
 * interface
 */
public interface SchedulingMaster extends PolicyMaster {
    /**
     * Add an {@link FutureEvent#ESCALONAR ESCALONAR}-type event associated with this master to the
     * {@link ispd.motor.Simulation Simulation}'s queue.
     *
     * @see FutureEvent
     * @see ispd.motor.Simulation
     * @see ispd.motor.Simulation#addFutureEvent(FutureEvent)
     */
    void executeScheduling();

    /**
     * Updates the set of {@link PolicyCondition} under which this {@code SchedulingMaster} instance
     * should execute scheduling operations. The given set may be any combination of
     * {@link PolicyCondition}s; it may include repetition or even be empty.
     * <p>
     * Some specifics about the implementation, in particular
     * {@link ispd.motor.filas.servidores.implementacao.CS_Mestre CS_Mestre} and
     * {@link ispd.motor.filas.servidores.implementacao.CS_VMM CS_VMM}'s:
     * <ul>
     *     <li>The {@link Set} passed as an argument is kept via reference.</li>
     *     <li>The {@link Set} is not modified in any way by the {@code SchedulingMaster}.</li>
     *     <li>Therefore, different {@code SchedulingMaster}s may share the same collection
     *     instance safely.</li>
     * </ul>
     * <hr>
     * <b>Usage:</b>
     * <p>
     * This method should be called <b>at most once</b>, and <i>strictly</i> before a master starts
     * its scheduling. Violating these expectations will result in unpredictable behavior.
     * <p>
     * Callers of this method should guarantee that any collections given as policy conditions
     * <b>are not</b> modified while they are "in use". An example follows:
     *
     * <pre> {@code
     *      SchedulingMaster master = ...;
     *      SchedulingMaster otherMaster = ...;
     *      Set<PolicyCondition> conditions = Set.of(PolicyCondition.WHEN_RECEIVES_RESULT);
     *      master.setSchedulingConditions(conditions);
     *
     *      // While using master for scheduling
     *      // conditions.add(PolicyCondition.WHILE_MUST_DISTRIBUTE); -- unpredictable behavior!
     *
     *      var otherConditions = PolicyConditionSets.ALL;
     *      otherMaster.setSchedulingConditions(otherConditions);
     *
     *      // While using otherMaster for scheduling
     *      conditions.add(PolicyCondition.WHILE_MUST_DISTRIBUTE); // -- okay, no longer in use
     * }</pre>
     * <p>
     * It is extremely advised to use only pre-created {@link PolicyCondition} sets, available as
     * static fields in the class {@link PolicyConditionSets PolicyConditionSets}. The
     * collections offered by that class are wrapped in
     * {@link java.util.Collections#unmodifiableSet(Set) unmodifiable sets}, adding a degree of
     * safety to avoid unpredictable behavior.
     *
     * @param newConditions the new set of {@link PolicyCondition}s to be associated with this
     *                      {@code SchedulingMaster}.
     *
     * @see PolicyCondition
     * @see PolicyConditionSets
     */
    void setSchedulingConditions(Set<PolicyCondition> newConditions);

    /**
     * Sends the specified {@link Tarefa} (task) to the appropriate destination for processing.
     * <p>
     * Creates a new event of type {@link FutureEvent#SAIDA SAIDA} (of the task leaving this
     * resource) in the {@link ispd.motor.Simulation Simulation}'s event queue.
     *
     * @param task the {@link Tarefa} to be sent for processing.
     *
     * @see FutureEvent
     * @see ispd.motor.Simulation
     * @see ispd.motor.Simulation#addFutureEvent(FutureEvent)
     */
    void sendTask(Tarefa task);

    /**
     * Creates and returns a copy of the specified {@link Tarefa} (task).
     * <p>
     * Updates the running {@link ispd.motor.Simulation Simulation} with the added task via its
     * {@link ispd.motor.Simulation#addJob(Tarefa) addJob} method.
     *
     * @param task the {@link Tarefa} to be copied.
     *
     * @return a copy of the specified {@link Tarefa}.
     *
     * @see ispd.motor.Simulation
     * @see ispd.motor.Simulation#addJob(Tarefa)
     */
    Tarefa cloneTask(Tarefa task);

    /**
     * Creates a {@link ispd.motor.filas.Mensagem Mensagem} of the specified type, related to the
     * specified {@link Tarefa} (task), with destination set to the specified
     * {@link CS_Processamento} (slave). Then, adds the created message with an associated
     * {@link FutureEvent#MENSAGEM MENSAGEM event} to the {@link ispd.motor.Simulation Simulation}'s
     * event queue.
     *
     * @param task        the {@link Tarefa} related to the message.
     * @param slave       the {@link CS_Processamento} that should receive the message.
     * @param messageType the type of message to be sent.
     *
     * @see ispd.motor.filas.Mensagem
     * @see FutureEvent
     * @see ispd.motor.Simulation
     * @see ispd.motor.Simulation#addFutureEvent(FutureEvent)
     */
    void sendMessage(Tarefa task, CS_Processamento slave, int messageType);

    /**
     * Updates the specified {@link CS_Processamento} (slave) with the most recent
     * scheduling-related information.
     * <p>
     * Creates a {@link ispd.motor.filas.Mensagem Mensagem} of type
     * {@link Mensagens#ATUALIZAR ATUALIZAR},with destination set to the specified
     * {@link CS_Processamento}, and adds a {@link FutureEvent#MENSAGEM MENSAGEM} event with the
     * generated message to the {@link ispd.motor.Simulation Simulation}'s event queue.
     *
     * @param slave the {@link CS_Processamento} to be updated.
     *
     * @see ispd.motor.filas.Mensagem
     * @see FutureEvent
     * @see ispd.motor.Simulation
     * @see ispd.motor.Simulation#addFutureEvent(FutureEvent)
     */
    @SuppressWarnings("unused")
    // Used 'reflectively' in gerador/Interpretador.java
    void updateSubordinate(CS_Processamento slave);
}
