package ispd.policy.scheduling.grid;

import ispd.policy.scheduling.SchedulingPolicy;

/**
 * The {@code GridSchedulingPolicy} class is an abstract specialization of {@link SchedulingPolicy}
 * for the representation of task scheduling algorithms in grid computing systems.
 * <p>
 * It specializes the parameterization of {@link SchedulingPolicy} with the {@link GridMaster}
 * interface, and does not provide any additional methods.
 * <p>
 * This class does not have further specializations to differentiate policy types, only concrete
 * implementations. They can be found in the {@link ispd.policy.scheduling.grid.impl impl}
 * subpackage.
 *
 * @see SchedulingPolicy The superclass this one specializes
 * @see GridMaster The PolicyMaster specialization this class is associated with
 */
public abstract class GridSchedulingPolicy extends SchedulingPolicy<GridMaster> {
}
