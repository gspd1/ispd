/**
 * The {@code grid} package contains:
 * <ul>
 *     <li>{@link ispd.policy.scheduling.grid.GridMaster GridMaster}: A specialization
 *     of {@link ispd.policy.PolicyMaster PolicyMaster} for grid-based scheduling.</li>
 *     <li>{@link ispd.policy.scheduling.grid.GridSchedulingPolicy GridSchedulingPolicy}: A
 *     specialization of {@link ispd.policy.scheduling.SchedulingPolicy SchedulingPolicy} for
 *     grid-based scheduling.</li>
 *     <li>The subpackage {@link ispd.policy.scheduling.grid.impl impl}, which holds
 *     implementations for concrete grid-based scheduling policies.</li>
 * </ul>
 */
package ispd.policy.scheduling.grid;