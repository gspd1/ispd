package ispd.policy.scheduling.grid.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.Mensagens;
import ispd.motor.filas.Mensagem;
import ispd.motor.filas.Tarefa;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.policy.PolicyConditionSets;
import ispd.policy.scheduling.grid.GridSchedulingPolicy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@ConcretePolicy
public class OSEP extends GridSchedulingPolicy {
    private final List<ControleEscravos> controleEscravos = new ArrayList<>();
    private final List<Tarefa> esperaTarefas = new ArrayList<>();
    private final List<ControlePreempcao> controlePreempcao = new ArrayList<>();
    private final List<List<Tarefa>> processadorEscravos = new ArrayList<>();
    private Tarefa tarefaSelec = null;
    private HashMap<String, StatusUser> status = null;
    private int contadorEscravos = 0;

    public OSEP() {
        this.tasks = new ArrayList<>();
        this.slaves = new ArrayList<>();
        this.slavesTaskQueues = new ArrayList<>();
    }

    @Override
    public void initialize() {
        this.policyMaster.setSchedulingConditions(PolicyConditionSets.ALL);//Escalonamento quando
        // chegam this.tasks e quando this.tasks são concluídas
        this.status = new HashMap<>();

        for (int i = 0; i < this.userMetrics.getUsuarios().size(); i++) {//Objetos de controle de
            // uso e cota para cada um dos usuários
            this.status.put(this.userMetrics.getUsuarios().get(i),
                    new StatusUser(this.userMetrics.getUsuarios().get(i)));
        }

        for (final CS_Processamento slave : this.slaves) {//Contadores para lidar com a dinamicidade
            // dos dados
            this.controleEscravos.add(new ControleEscravos(slave.getId()));
            this.slavesTaskQueues.add(new ArrayList<>());
            this.processadorEscravos.add(new ArrayList<>());
        }
    }

    @Override
    public void schedule() {
        final Tarefa trf = this.scheduleTask();
        if (trf != null) {
            this.tarefaSelec = trf;
            final StatusUser estado = this.status.get(trf.getProprietario());
            final CS_Processamento rec = this.scheduleResource();
            if (rec != null) {
                trf.setLocalProcessamento(rec);
                trf.setCaminho(this.scheduleRoute(rec));
                //Verifica se não é caso de preempção
                if ("Livre".equals(this.controleEscravos.get(this.slaves.indexOf(rec)).getStatus())) {
                    estado.rmDemanda();
                    estado.addServedNum();
                    this.controleEscravos.get(this.slaves.indexOf(rec)).setBloqueado();
                    this.policyMaster.sendTask(trf);
                } else {

                    if ("Ocupado".equals(
                            this.controleEscravos.get(this.slaves.indexOf(rec)).getStatus())) {
                        final int index_rec = this.slaves.indexOf(rec);
                        this.esperaTarefas.add(trf);
                        this.controlePreempcao.add(new ControlePreempcao((this.processadorEscravos.get(index_rec).get(0)).getProprietario(), (this.processadorEscravos.get(index_rec).get(0)).getIdentificador(), trf.getProprietario(), trf.getIdentificador()));
                        this.controleEscravos.get(this.slaves.indexOf(rec)).setBloqueado();
                    }
                }
            } else {
                this.tasks.add(trf);
                this.tarefaSelec = null;
            }
        }
    }

    @Override
    public Tarefa scheduleTask() {
        //Usuários com maior diferença entre uso e posse terão preferência
        int difUsuarioMinimo = -1;
        int indexUsuarioMinimo = -1;
        String user;
        //Encontrar o usuário que está mais abaixo da sua propriedade
        for (int i = 0; i < this.userMetrics.getUsuarios().size(); i++) {
            user = this.userMetrics.getUsuarios().get(i);

            //Caso existam this.tasks do usuário corrente e ele esteja com uso menor que sua posse
            if ((this.status.get(user).getServedNum() < this.status.get(user).getOwnerShare()) && this.status.get(user).getDemanda() > 0) {
                if (difUsuarioMinimo == -1) {
                    difUsuarioMinimo =
                            this.status.get(user).getOwnerShare() - this.status.get(user).getServedNum();
                    indexUsuarioMinimo = i;
                } else {
                    if (difUsuarioMinimo < this.status.get(user).getOwnerShare() - this.status.get(user).getServedNum()) {
                        difUsuarioMinimo =
                                this.status.get(user).getOwnerShare() - this.status.get(user).getServedNum();
                        indexUsuarioMinimo = i;
                    }
                }
            }
        }

        if (indexUsuarioMinimo != -1) {
            int indexTarefa = -1;

            for (int i = 0; i < this.tasks.size(); i++) {
                if (this.tasks.get(i).getProprietario().equals(this.userMetrics.getUsuarios().get(indexUsuarioMinimo))) {
                    if (indexTarefa == -1) {
                        indexTarefa = i;
                        break;
                    }
                }
            }

            if (indexTarefa != -1) {
                return this.tasks.remove(indexTarefa);
            }
        }

        if (!this.tasks.isEmpty()) {
            return this.tasks.remove(0);
        } else {
            return null;
        }
    }

    @Override
    public void updateResult(final Mensagem message) {
        super.updateResult(message);
        final int index = this.slaves.indexOf(message.getOrigem());
        this.processadorEscravos.set(index, message.getProcessadorEscravo());
        this.contadorEscravos++;
        if (this.contadorEscravos == this.slaves.size()) {
            boolean escalona = false;
            for (int i = 0; i < this.slaves.size(); i++) {
                if ("Bloqueado".equals(this.controleEscravos.get(i).getStatus())) {
                    this.controleEscravos.get(i).setIncerto();
                }
                if ("Incerto".equals(this.controleEscravos.get(i).getStatus())) {
                    if (this.processadorEscravos.get(i).isEmpty()) {
                        this.controleEscravos.get(i).setLivre();
                        escalona = true;
                    }
                    if (this.processadorEscravos.size() == 1) {
                        this.controleEscravos.get(i).setOcupado();
                    }
                    if (this.processadorEscravos.size() > 1) {
                    }
                }
            }
            this.contadorEscravos = 0;
            if (!this.tasks.isEmpty() && escalona) {
                this.policyMaster.executeScheduling();
            }
        }
    }

    @Override
    public void submitTask(final Tarefa newTask) {
        super.submitTask(newTask);
        final CS_Processamento maq = (CS_Processamento) newTask.getLocalProcessamento();
        final StatusUser estadoUser = this.status.get(newTask.getProprietario());
        //Em caso de preempção, é procurada a tarefa correspondente para ser enviada ao escravo
        // agora desocupado
        if (newTask.getLocalProcessamento() != null) {

            int j;
            int indexControle = -1;
            for (j = 0; j < this.controlePreempcao.size(); j++) {
                if (this.controlePreempcao.get(j).getPreempID() == newTask.getIdentificador() && this.controlePreempcao.get(j).getUsuarioPreemp().equals(newTask.getProprietario())) {
                    indexControle = j;
                    break;
                }
            }

            for (int i = 0; i < this.esperaTarefas.size(); i++) {
                if (this.esperaTarefas.get(i).getProprietario().equals(this.controlePreempcao.get(indexControle).getUsuarioAlloc()) && this.esperaTarefas.get(i).getIdentificador() == this.controlePreempcao.get(j).getAllocID()) {

                    this.policyMaster.sendTask(this.esperaTarefas.get(i));

                    this.status.get(this.controlePreempcao.get(indexControle).getUsuarioAlloc()).addServedNum();

                    this.status.get(this.controlePreempcao.get(indexControle).getUsuarioPreemp()).addDemanda();
                    this.status.get(this.controlePreempcao.get(indexControle).getUsuarioPreemp()).rmServedNum();

                    this.controleEscravos.get(this.slaves.indexOf(maq)).setBloqueado();

                    this.esperaTarefas.remove(i);
                    this.controlePreempcao.remove(j);
                    break;
                }
            }

        } else {
            this.policyMaster.executeScheduling();
            estadoUser.addDemanda();
        }
    }

    @Override
    public void addCompletedTask(final Tarefa completedTask) {
        super.addCompletedTask(completedTask);
        final CS_Processamento maq = (CS_Processamento) completedTask.getLocalProcessamento();
        final StatusUser estado = this.status.get(completedTask.getProprietario());

        estado.rmServedNum();
        final int index = this.slaves.indexOf(maq);
        this.controleEscravos.get(index).setLivre();
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destination) {
        final int index = this.slaves.indexOf(destination);
        return new ArrayList<>(this.slaveRoutes.get(index));
    }

    @Override
    public CS_Processamento scheduleResource() {
        String user;
        //Buscando recurso livre
        CS_Processamento selec = null;

        for (int i = 0; i < this.slaves.size(); i++) {

            if ("Livre".equals(this.controleEscravos.get(i).getStatus())) {//Garantir que o escravo
                // está de fato livre e que não há nenhuma tarefa em trânsito para o escravo
                if (selec == null) {
                    selec = this.slaves.get(i);
                    break;
                }
            }
        }

        if (selec != null) {
            return selec;
        }

        String usermax = null;
        int diff = -1;

        for (int i = 0; i < this.userMetrics.getUsuarios().size(); i++) {
            user = this.userMetrics.getUsuarios().get(i);
            if (this.status.get(user).getServedNum() > this.status.get(user).getOwnerShare() && !user.equals(this.tarefaSelec.getProprietario())) {
                if (diff == -1) {
                    usermax = this.userMetrics.getUsuarios().get(i);
                    diff = this.status.get(user).getServedNum() - this.status.get(user).getOwnerShare();
                } else {
                    if (this.status.get(user).getServedNum() - this.status.get(user).getOwnerShare() > diff) {
                        usermax = user;
                        diff = this.status.get(user).getServedNum() - this.status.get(user).getOwnerShare();
                    }
                }
            }
        }

        int index = -1;
        if (usermax != null) {

            for (int i = 0; i < this.slaves.size(); i++) {
                if ("Ocupado".equals(this.controleEscravos.get(i).getStatus()) && (this.processadorEscravos.get(i).get(0)).getProprietario().equals(usermax)) {
                    index = i;
                    break;
                }
            }
        }

        //Fazer a preempção
        if (index != -1) {
            selec = this.slaves.get(index);
            final int index_selec = this.slaves.indexOf(selec);
            this.policyMaster.sendMessage(this.processadorEscravos.get(index_selec).get(0),
                    selec, Mensagens.DEVOLVER_COM_PREEMPCAO);
            return selec;
        }
        return null;
    }

    @Override
    public Double getUpdateInterval() {
        return 15.0;
    }

    private static class ControleEscravos {
        private final String ID;//Id da máquina escravo
        private String status;//Estado da máquina

        private ControleEscravos(final String ID) {
            this.status = "Livre";
            this.ID = ID;
        }

        private String getStatus() {
            return this.status;
        }

        private void setOcupado() {
            this.status = "Ocupado";
        }

        private void setLivre() {
            this.status = "Livre";
        }

        private void setBloqueado() {
            this.status = "Bloqueado";
        }

        private void setIncerto() {
            this.status = "Incerto";
        }
    }

    private static class ControlePreempcao {
        private final String usuarioPreemp;
        private final String usuarioAlloc;
        private final int preempID;//ID da tarefa que sofreu preempção
        private final int allocID;//ID da tarefa alocada

        private ControlePreempcao(final String user1, final int pID, final String user2,
                                  final int aID) {
            this.usuarioPreemp = user1;
            this.preempID = pID;
            this.usuarioAlloc = user2;
            this.allocID = aID;
        }

        private String getUsuarioPreemp() {
            return this.usuarioPreemp;
        }

        private int getPreempID() {
            return this.preempID;
        }

        private String getUsuarioAlloc() {
            return this.usuarioAlloc;
        }

        private int getAllocID() {
            return this.allocID;
        }
    }

    private class StatusUser {
        private int demanda;//Número de this.tasks na fila
        private int ownerShare;//Número de máquinas do usuario
        private int servedNum;//Número de máquinas que atendem ao usuário

        private StatusUser(final String user) {
            this.demanda = 0;
            this.ownerShare = 0;
            this.servedNum = 0;

            int i;
            int j = 0;
            for (i = 0; i < OSEP.this.slaves.size(); i++) {
                if (OSEP.this.slaves.get(i).getProprietario().equals(user)) {
                    j++;
                }
            }
            this.ownerShare = j;
        }

        private void addDemanda() {
            this.demanda++;
        }

        private void rmDemanda() {
            this.demanda--;
        }

        private void addServedNum() {
            this.servedNum++;
        }

        private void rmServedNum() {
            this.servedNum--;
        }

        private int getDemanda() {
            return this.demanda;
        }

        private int getOwnerShare() {
            return this.ownerShare;
        }

        private int getServedNum() {
            return this.servedNum;
        }
    }
}
