/**
 * This package contains the following concrete implementations of grid-based scheduling policies:
 * <ul>
 *     <li>{@link ispd.policy.scheduling.grid.impl.RoundRobin RoundRobin}</li>
 *     <li>{@link ispd.policy.scheduling.grid.impl.Workqueue Workqueue}</li>
 *     <li>{@link ispd.policy.scheduling.grid.impl.WQR WQR}</li>
 *     <li>{@link ispd.policy.scheduling.grid.impl.DynamicFPLTF DynamicFPLTF}</li>
 *     <li>{@link ispd.policy.scheduling.grid.impl.OSEP OSEP}</li>
 *     <li>{@link ispd.policy.scheduling.grid.impl.M_OSEP M_OSEP}</li>
 *     <li>{@link ispd.policy.scheduling.grid.impl.HOSEP HOSEP}</li>
 *     <li>{@link ispd.policy.scheduling.grid.impl.EHOSEP EHOSEP}</li>
 * </ul>
 */
package ispd.policy.scheduling.grid.impl;