package ispd.policy.scheduling.grid.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.Mensagens;
import ispd.motor.filas.Mensagem;
import ispd.motor.filas.Tarefa;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.policy.PolicyConditionSets;
import ispd.policy.scheduling.grid.GridMaster;
import ispd.policy.scheduling.grid.GridSchedulingPolicy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ConcretePolicy
public class HOSEP extends GridSchedulingPolicy {
    private final List<StatusUser> status = new ArrayList<>();
    private final List<ControleEscravos> controleEscravos = new ArrayList<>();
    private final List<Tarefa> esperaTarefas = new ArrayList<>();
    private final List<ControlePreempcao> controlePreempcao = new ArrayList<>();

    public HOSEP() {
        this.tasks = new ArrayList<>();
        this.slaves = new ArrayList<>();
        this.slavesTaskQueues = new ArrayList<>();
    }

    @Override
    public void initialize() {
        //Escalonamento quando chegam tarefas e quando tarefas são concluídas
        this.policyMaster.setSchedulingConditions(PolicyConditionSets.ALL);

        //Objetos de controle de uso e cota para cada um dos usuários
        for (int i = 0; i < this.userMetrics.getUsuarios().size(); i++) {
            //Calcular o poder computacional da porção de cada usuário
            double poderComp = 0.0;
            for (final CS_Processamento slave : this.slaves) {
                //Se o nó corrente não é mestre e pertence ao usuário corrente
                if (!(slave instanceof GridMaster) && slave.getProprietario().equals(this.userMetrics.getUsuarios().get(i))) {
                    //Calcular o poder total da porcao do usuário corrente
                    poderComp += slave.getPoderComputacional();
                }
            }
            //Adiciona dados do usuário corrente à lista 
            this.status.add(new StatusUser(this.userMetrics.getUsuarios().get(i), poderComp));
        }

        //Controle dos nós, com cópias das filas de cada um e da tarefa que executa em cada um
        for (int i = 0; i < this.slaves.size(); i++) {
            this.controleEscravos.add(new ControleEscravos(new ArrayList<>()));
        }
    }

    @Override
    public void schedule() {
        int indexTarefa;
        int indexEscravo;
        StatusUser cliente;

        //Ordenar os usuários em ordem crescente de Poder Remanescente
        Collections.sort(this.status);

        for (final StatusUser statusUser : this.status) {
            cliente = statusUser;

            //Buscar tarefa para execucao
            indexTarefa = this.buscarTarefa(cliente);

            if (indexTarefa != -1) {

                //Buscar máquina para executar a tarefa definida
                indexEscravo = this.buscarRecurso(cliente);

                if (indexEscravo != -1) {

                    //Se não é caso de preempção, a tarefa é configurada e enviada
                    if ("Livre".equals(this.controleEscravos.get(indexEscravo).getStatus())) {

                        final Tarefa tar = this.tasks.remove(indexTarefa);
                        tar.setLocalProcessamento(this.slaves.get(indexEscravo));
                        tar.setCaminho(this.scheduleRoute(this.slaves.get(indexEscravo)));
                        this.policyMaster.sendTask(tar);

                        //Atualização dos dados sobre o usuário
                        cliente.rmDemanda();
                        cliente.addServedPerf(this.slaves.get(indexEscravo).getPoderComputacional());

                        //Controle das máquinas
                        this.controleEscravos.get(indexEscravo).setBloqueado();
                        return;
                    }

                    //Se é caso de preempção, a tarefa configurada e colocada em espera
                    if ("Ocupado".equals(this.controleEscravos.get(indexEscravo).getStatus())) {

                        final Tarefa tar = this.tasks.remove(indexTarefa);
                        tar.setLocalProcessamento(this.slaves.get(indexEscravo));
                        tar.setCaminho(this.scheduleRoute(this.slaves.get(indexEscravo)));

                        //Controle de preempção para enviar a nova tarefa no momento certo
                        final String userPreemp =
                                this.controleEscravos.get(indexEscravo).GetProcessador().get(0).getProprietario();
                        final int idTarefaPreemp =
                                this.controleEscravos.get(indexEscravo).GetProcessador().get(0).getIdentificador();
                        this.controlePreempcao.add(new ControlePreempcao(userPreemp,
                                idTarefaPreemp, tar.getProprietario(), tar.getIdentificador()));
                        this.esperaTarefas.add(tar);

                        //Solicitação de retorno da tarefa em execução e atualização da demanda
                        // do usuário
                        this.policyMaster.sendMessage(this.controleEscravos.get(indexEscravo).GetProcessador().get(0), this.slaves.get(indexEscravo), Mensagens.DEVOLVER_COM_PREEMPCAO);
                        this.controleEscravos.get(indexEscravo).setBloqueado();
                        cliente.rmDemanda();
                        return;
                    }
                }
            }
        }
    }

    private int buscarTarefa(final StatusUser usuario) {

        //Indice da tarefa na lista de tarefas
        int trf = -1;
        //Se o usuario tem demanda nao atendida e seu consumo nao chegou ao limite
        if (usuario.getDemanda() > 0) {
            //Procura pela menor tarefa nao atendida do usuario.
            for (int j = 0; j < this.tasks.size(); j++) {
                if (this.tasks.get(j).getProprietario().equals(usuario.getNome())) {
                    if (trf == -1) {
                        trf = j;
                    } else if (this.tasks.get(j).getTamProcessamento() < this.tasks.get(trf).getTamProcessamento()) {//Escolher a tarefa de menor tamanho do usuario
                        trf = j;
                    }
                }
            }
        }
        return trf;
    }

    private int buscarRecurso(final StatusUser cliente) {

        /*++++++++++++++++++Buscando recurso livres++++++++++++++++++*/

        //Índice da máquina escolhida, na lista de máquinas
        int indexSelec = -1;

        for (int i = 0; i < this.slaves.size(); i++) {

            if ("Livre".equals(this.controleEscravos.get(i).getStatus())) {
                if (indexSelec == -1) {
                    indexSelec = i;
                } else if (this.slaves.get(i).getPoderComputacional() > this.slaves.get(indexSelec).getPoderComputacional()) {
                    indexSelec = i;
                }
            }
        }

        if (indexSelec != -1) {
            return indexSelec;
        }

        /*+++++++++++++++++Busca por usuário para preempção+++++++++++++++++*/

        if (this.status.get(this.status.size() - 1).getServedPerf() > this.status.get(this.status.size() - 1).getPerfShare() && cliente.getServedPerf() < cliente.getPerfShare()) {

            for (int i = 0; i < this.slaves.size(); i++) {

                if ("Ocupado".equals(this.controleEscravos.get(i).getStatus())) {
                    if (this.controleEscravos.get(i).GetProcessador().get(0).getProprietario().equals(this.status.get(this.status.size() - 1).getNome())) {

                        if (indexSelec == -1) {

                            indexSelec = i;

                        } else {
                            if (this.slaves.get(i).getPoderComputacional() < this.slaves.get(indexSelec).getPoderComputacional()) {

                                indexSelec = i;
                            }
                        }
                    }
                }
            }

            if (indexSelec != -1) {

                final double penalidaUserEsperaPosterior =
                        (cliente.getServedPerf() + this.slaves.get(indexSelec).getPoderComputacional() - cliente.getPerfShare()) / cliente.getPerfShare();
                final double penalidaUserEscravoPosterior =
                        (this.status.get(this.status.size() - 1).getServedPerf() - this.slaves.get(indexSelec).getPoderComputacional() - this.status.get(this.status.size() - 1).getPerfShare()) / this.status.get(this.status.size() - 1).getPerfShare();

                if (penalidaUserEscravoPosterior >= penalidaUserEsperaPosterior || penalidaUserEscravoPosterior > 0) {
                    return indexSelec;
                } else {
                    return -1;
                }
            }
        }
        return indexSelec;
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destination) {
        final int index = this.slaves.indexOf(destination);
        return new ArrayList<>(this.slaveRoutes.get(index));
    }

    //Metodo necessario para implementar interface. Não é usado.
    @Override
    public CS_Processamento scheduleResource() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    //Definir o intervalo de tempo, em segundos, em que as máquinas enviarão dados de atualização
    // para o escalonador
    @Override
    public Double getUpdateInterval() {
        return 15.0;
    }

    //Metodo necessario para implementar interface. Não é usado.
    @Override
    public Tarefa scheduleTask() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateResult(final Mensagem message) {
        //Localizar máquina que enviou estado atualizado
        final int index = this.slaves.indexOf(message.getOrigem());

        //Atualizar listas de espera e processamento da máquina
        this.controleEscravos.get(index).setProcessador(message.getProcessadorEscravo());

        //Tanto alocação para recurso livre como a preempção levam dois ciclos de atualização
        // para que a máquina possa ser considerada para esacalonamento novamente

        //Primeiro ciclo
        if ("Bloqueado".equals(this.controleEscravos.get(index).getStatus())) {
            this.controleEscravos.get(index).setIncerto();
            //Segundo ciclo
        } else if ("Incerto".equals(this.controleEscravos.get(index).getStatus())) {
            //Se não está executando nada
            if (this.controleEscravos.get(index).GetProcessador().isEmpty()) {

                this.controleEscravos.get(index).setLivre();
                //Se está executando uma tarefa
            } else if (this.controleEscravos.get(index).GetProcessador().size() == 1) {

                this.controleEscravos.get(index).setOcupado();
                //Se há mais de uma tarefa e a máquina tem mais de um núcleo
            }
        }
    }

    @Override
    //Receber nova tarefa submetida ou tarefa que sofreu preemoção
    public void submitTask(final Tarefa tarefa) {

        //Método herdado, obrigatório executar para obter métricas ao final da slimuação
        super.submitTask(tarefa);

        //Atualização da demanda do usuário proprietário da tarefa
        for (final StatusUser statusUser : this.status) {
            if (statusUser.getNome().equals(tarefa.getProprietario())) {
                statusUser.addDemanda();
                break;
            }
        }

        //Em caso de preempção
        if (tarefa.getLocalProcessamento() != null) {

            //Localizar informações de estado de máquina que executou a tarefa (se houver)
            final CS_Processamento maq = (CS_Processamento) tarefa.getLocalProcessamento();

            //Localizar informações armazenadas sobre a preempção em particular

            int indexControlePreemp = -1;
            int indexStatusUserAlloc = -1;
            int indexStatusUserPreemp = -1;

            for (int j = 0; j < this.controlePreempcao.size(); j++) {
                if (this.controlePreempcao.get(j).getPreempID() == tarefa.getIdentificador() && this.controlePreempcao.get(j).getUsuarioPreemp().equals(tarefa.getProprietario())) {
                    indexControlePreemp = j;
                    break;
                }
            }

            for (int k = 0; k < this.status.size(); k++) {
                if (this.status.get(k).getNome().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioAlloc())) {
                    indexStatusUserAlloc = k;
                    break;
                }
            }

            for (int k = 0; k < this.status.size(); k++) {
                if (this.status.get(k).getNome().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioPreemp())) {
                    indexStatusUserPreemp = k;
                    break;
                }
            }

            //Localizar tarefa em espera deseignada para executar
            for (int i = 0; i < this.esperaTarefas.size(); i++) {

                if (this.esperaTarefas.get(i).getProprietario().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioAlloc()) && this.esperaTarefas.get(i).getIdentificador() == this.controlePreempcao.get(indexControlePreemp).getAllocID()) {

                    //Enviar tarefa para execução
                    this.policyMaster.sendTask(this.esperaTarefas.remove(i));

                    //Atualizar informações de estado do usuário cuja tarefa será executada
                    this.status.get(indexStatusUserAlloc).addServedPerf(maq.getPoderComputacional());

                    //Atualizar informações de estado do usuáro cuja tarefa foi interrompida
                    this.status.get(indexStatusUserPreemp).rmServedPerf(maq.getPoderComputacional());

                    //Com a preempção feita, os dados necessários para ela são eliminados
                    this.controlePreempcao.remove(indexControlePreemp);
                    //Encerrar laço
                    break;
                }
            }
        }
    }

    @Override
    //Chegada de tarefa concluida
    public void addCompletedTask(final Tarefa tarefa) {
        //Método herdado, obrigatório executar para obter métricas ao final da simulação
        super.addCompletedTask(tarefa);

        //Localizar informações sobre máquina que executou a tarefa e usuário proprietário da tarefa
        final CS_Processamento maq = (CS_Processamento) tarefa.getLocalProcessamento();
        final int maqIndex = this.slaves.indexOf(maq);

        if ("Ocupado".equals(this.controleEscravos.get(maqIndex).getStatus())) {

            int statusIndex = -1;

            for (int i = 0; i < this.status.size(); i++) {
                if (this.status.get(i).getNome().equals(tarefa.getProprietario())) {
                    statusIndex = i;
                }
            }

            //Atualização das informações de estado do proprietario da tarefa terminada.
            this.status.get(statusIndex).rmServedPerf(maq.getPoderComputacional());
            this.controleEscravos.get(maqIndex).setLivre();

        } else if ("Bloqueado".equals(this.controleEscravos.get(maqIndex).getStatus())) {

            int indexControlePreemp = -1;
            int indexStatusUserAlloc = -1;
            int indexStatusUserPreemp = -1;

            for (int j = 0; j < this.controlePreempcao.size(); j++) {
                if (this.controlePreempcao.get(j).getPreempID() == tarefa.getIdentificador() && this.controlePreempcao.get(j).getUsuarioPreemp().equals(tarefa.getProprietario())) {
                    indexControlePreemp = j;
                    break;
                }
            }

            for (int k = 0; k < this.status.size(); k++) {
                if (this.status.get(k).getNome().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioAlloc())) {
                    indexStatusUserAlloc = k;
                    break;
                }
            }

            for (int k = 0; k < this.status.size(); k++) {
                if (this.status.get(k).getNome().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioPreemp())) {
                    indexStatusUserPreemp = k;
                    break;
                }
            }

            //Localizar tarefa em espera designada para executar
            for (int i = 0; i < this.esperaTarefas.size(); i++) {
                if (this.esperaTarefas.get(i).getProprietario().equals(this.controlePreempcao.get(indexControlePreemp).getUsuarioAlloc()) && this.esperaTarefas.get(i).getIdentificador() == this.controlePreempcao.get(indexControlePreemp).getAllocID()) {

                    //Enviar tarefa para execução
                    this.policyMaster.sendTask(this.esperaTarefas.remove(i));

                    //Atualizar informações de estado do usuário cuja tarefa será executada
                    this.status.get(indexStatusUserAlloc).addServedPerf(maq.getPoderComputacional());

                    //Atualizar informações de estado do usuário cuja tarefa teve a execução
                    // interrompida
                    this.status.get(indexStatusUserPreemp).rmServedPerf(maq.getPoderComputacional());

                    //Com a preempção feita, os dados necessários para ela são eliminados
                    this.controlePreempcao.remove(indexControlePreemp);
                    //Encerrar laço
                    break;
                }
            }
        }
    }

    //Classe para dados de estado dos usuários
    private static class StatusUser implements Comparable<StatusUser> {
        private final String user; //Nome do usuario
        private final double perfShare;//Desempenho total das máquinas do usuário
        private int demanda;//Número de tarefas na fila
        private double servedPerf;//Desempenho total que atende ao usuário

        private StatusUser(final String user, final double perfShare) {
            this.user = user;
            this.demanda = 0;
            this.perfShare = perfShare;
            this.servedPerf = 0.0;
        }

        private void addDemanda() {
            this.demanda++;
        }

        private void rmDemanda() {
            this.demanda--;
        }

        private void addServedPerf(final Double perf) {
            this.servedPerf += perf;
        }

        private void rmServedPerf(final Double perf) {
            this.servedPerf -= perf;
        }

        private String getNome() {
            return this.user;
        }

        private int getDemanda() {
            return this.demanda;
        }

        //Comparador para ordenação
        @Override
        public int compareTo(final StatusUser o) {
            if (((this.servedPerf - this.perfShare) / this.perfShare) < ((o.getServedPerf() - o.getPerfShare()) / o.getPerfShare())) {
                return -1;
            }
            if (((this.servedPerf - this.perfShare) / this.perfShare) > ((o.getServedPerf() - o.getPerfShare()) / o.getPerfShare())) {
                return 1;
            }
            if (this.perfShare >= o.getPerfShare()) {
                return -1;
            } else {
                return 1;
            }
        }

        private double getPerfShare() {
            return this.perfShare;
        }

        private double getServedPerf() {
            return this.servedPerf;
        }
    }


    //Classe para arnazenar o estado das máquinas no sistema
    private static class ControleEscravos {
        private String status;//Estado da máquina
        private List<Tarefa> processador;

        private ControleEscravos(final ArrayList<Tarefa> P) {
            this.status = "Livre";
            this.processador = P;
        }

        private List<Tarefa> GetProcessador() {
            return this.processador;
        }

        private String getStatus() {
            return this.status;
        }

        private void setProcessador(final List<Tarefa> P) {
            this.processador = P;
        }

        private void setOcupado() {
            this.status = "Ocupado";
        }

        private void setLivre() {
            this.status = "Livre";
        }

        private void setBloqueado() {
            this.status = "Bloqueado";
        }

        private void setIncerto() {
            this.status = "Incerto";
        }
    }

    //Classe para armazenar dados sobre as preempções que ainda não terminaram
    private static class ControlePreempcao {
        private final String usuarioPreemp;
        private final String usuarioAlloc;
        private final int preempID;//ID da tarefa que sofreu preempção
        private final int allocID;//ID da tarefa alocada

        private ControlePreempcao(
                final String user1, final int pID,
                final String user2, final int aID) {
            this.usuarioPreemp = user1;
            this.preempID = pID;
            this.usuarioAlloc = user2;
            this.allocID = aID;
        }

        private String getUsuarioPreemp() {
            return this.usuarioPreemp;
        }

        private int getPreempID() {
            return this.preempID;
        }

        private String getUsuarioAlloc() {
            return this.usuarioAlloc;
        }

        private int getAllocID() {
            return this.allocID;
        }
    }
}
