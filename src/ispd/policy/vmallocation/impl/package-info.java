/**
 * This package contains the following concrete implementations of VM allocation policies:
 * <ul>
 *     <li>{@link ispd.policy.vmallocation.impl.RoundRobin RoundRobin}</li>
 *     <li>{@link ispd.policy.vmallocation.impl.FirstFit FirstFit}</li>
 *     <li>{@link ispd.policy.vmallocation.impl.FirstFitDecreasing FirstFitDecreasing}</li>
 *     <li>{@link ispd.policy.vmallocation.impl.Volume Volume}</li>
 * </ul>
 * <p>
 * Furthermore, it contains an utilities package, {@link ispd.policy.vmallocation.impl.util util},
 * which holds {@link java.util.Comparator Comparator}s that some of these concrete
 * implementations depend upon.
 */
package ispd.policy.vmallocation.impl;