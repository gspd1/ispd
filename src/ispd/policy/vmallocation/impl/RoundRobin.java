package ispd.policy.vmallocation.impl;

import ispd.annotations.ConcretePolicy;
import ispd.motor.filas.servidores.CS_Processamento;
import ispd.motor.filas.servidores.CentroServico;
import ispd.motor.filas.servidores.implementacao.CS_MaquinaCloud;
import ispd.motor.filas.servidores.implementacao.CS_VMM;
import ispd.motor.filas.servidores.implementacao.CS_VirtualMac;
import ispd.policy.vmallocation.VmAllocationPolicy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

@ConcretePolicy
public class RoundRobin extends VmAllocationPolicy {
    private ListIterator<CS_Processamento> physicalMachine = null;

    public RoundRobin() {
        this.virtualMachines = new ArrayList<>(0);
        this.slaves = new LinkedList<>();
        this.rejectedVMs = new ArrayList<>(0);
    }

    @Override
    public void initialize() {
        this.physicalMachine = this.slaves.listIterator(0);

        if (this.virtualMachines.isEmpty()) {
            return;
        }

        this.schedule();
    }

    @Override
    public void schedule() {
        while (!(this.virtualMachines.isEmpty())) {
            this.findMachineForTask(this.scheduleVirtualMachine());
        }
    }

    @Override
    public List<CentroServico> scheduleRoute(final CentroServico destination) {
        final int index = this.slaves.indexOf(destination);
        return new ArrayList<>((List<CentroServico>) this.slaveRoutes.get(index));
    }

    @Override
    public CS_Processamento scheduleResource() {
        if (!this.physicalMachine.hasNext()) {
            this.physicalMachine = this.slaves.listIterator(0);
        }
        return this.physicalMachine.next();
    }

    @Override
    public CS_VirtualMac scheduleVirtualMachine() {
        return this.virtualMachines.remove(0);
    }

    private void findMachineForTask(final CS_VirtualMac vm) {
        int machines = this.slaves.size();
        while (machines >= 0) {
            if (machines == 0) {
                this.rejectVm(vm);
                machines--;
                continue;
            }

            final var resource = this.scheduleResource();

            if (resource instanceof CS_VMM) {
                this.redirectVm(vm, resource);
            } else {

                final var maq = (CS_MaquinaCloud) resource;
                final double memory = maq.getMemoriaDisponivel();
                final double neededMemory = vm.getMemoriaDisponivel();
                final double disk = maq.getDiscoDisponivel();
                final double neededDisk = vm.getDiscoDisponivel();
                final int processors = maq.getProcessadoresDisponiveis();
                final int procVM = vm.getProcessadoresDisponiveis();

                if ((neededMemory > memory || neededDisk > disk || procVM > processors)) {
                    machines--;
                    continue;
                }

                final double newMemory = memory - neededMemory;
                maq.setMemoriaDisponivel(newMemory);
                final double newDisk = disk - neededDisk;
                maq.setDiscoDisponivel(newDisk);
                final int newProcessors = processors - procVM;
                maq.setProcessadoresDisponiveis(newProcessors);
                vm.setMaquinaHospedeira((CS_MaquinaCloud) resource);
                vm.setCaminho(this.scheduleRoute(resource));
                this.policyMaster.sendVirtualMachine(vm);

            }

            return;
        }
    }

    private void rejectVm(final CS_VirtualMac auxVm) {
        auxVm.setStatus(CS_VirtualMac.REJEITADA);
        this.rejectedVMs.add(auxVm);
    }

    private void redirectVm(final CS_VirtualMac vm, final CS_Processamento resource) {
        vm.setCaminho(this.scheduleRoute(resource));
        this.policyMaster.sendVirtualMachine(vm);
    }
}
