package ispd.policy.vmallocation;

import ispd.motor.filas.servidores.implementacao.CS_VirtualMac;
import ispd.policy.Policy;

import java.util.List;

/**
 * The {@code VmAllocationPolicy} class is an abstract specialization of {@link Policy} for the
 * representation of VM allocation algorithms.
 * <p>
 * It specializes the parameterization of {@link Policy} with the {@link VirtualMachineMaster}
 * interface, and provides additional methods and fields that most allocation policies will rely
 * upon for their implementation.
 * <p>
 * This class does not have further specializations to differentiate policy types, only concrete
 * implementations. They can be found in the {@link ispd.policy.vmallocation.impl impl} subpackage.
 *
 * <hr>
 * Concrete subclasses of this class must provide an implementation for, besides the abstract
 * methods defined in {@link Policy}, the method {@link #scheduleVirtualMachine()}. This method
 * should be implemented to select a virtual machine to be allocated, based on the policy's logic.
 *
 * <hr>
 * The initialization of allocation policies follow the same guidelines as {@link Policy}'s, but
 * with further restrictions:
 * <ul>
 *     <li>Subclasses must initialize the fields {@link #virtualMachines} and
 *     {@link #rejectedVMs} with appropriate collections.</li>
 *     <li>{@link #addVirtualMachine} must be called to add each VM this policy's master will be
 *     responsible
 *     for.</li>
 * </ul>
 *
 * @see Policy The superclass this one specializes
 * @see VirtualMachineMaster The PolicyMaster specialization this class is associated with
 */
public abstract class VmAllocationPolicy extends Policy<VirtualMachineMaster> {
    /**
     * List of {@link CS_VirtualMac virtual machines} being managed by this policy.
     * <p>
     * When an allocation policy instance is created, its value is {@code null}. Subclasses
     * <b>must</b> initialize this field with an appropriate collection on their constructors.
     * <p>
     * VMs should then be added via the {@link #addVirtualMachine} method.
     * <p>
     * A reference to this field can be fetched with the {@link #getVirtualMachines} method.
     */
    protected List<CS_VirtualMac> virtualMachines = null;
    /**
     * List of {@link CS_VirtualMac virtual machines} that have had their allocation rejected.
     * <p>
     * When an allocation policy instance is created, its value is {@code null}. Subclasses
     * <b>must</b> initialize this field with an appropriate collection on their constructors.
     * <p>
     * A reference to this field can be fetched with the {@link #getRejectedVMs} method.
     */
    protected List<CS_VirtualMac> rejectedVMs = null;
    /**
     * A list of lists containing information about the virtual machines being managed by this
     * policy. The structure and content of the lists are determined by the implementing subclass.
     * <p>
     * When an allocation policy instance is created, its value is {@code null}. Subclasses may
     * choose to initialize this field with an appropriate collection on their constructors, if they
     * use it for their internal logic.
     */
    protected List<List> vmInfo = null;

    /**
     * Abstract method for scheduling a virtual machine for allocation into a physical resource.
     * <p>
     * Implementing subclasses should provide an algorithm for selecting a virtual machine from the
     * list of available virtual machines to be allocated.
     *
     * @return the {@link CS_VirtualMac} selected for allocation
     *
     * @throws IndexOutOfBoundsException if there are no available {@link CS_VirtualMac}s for
     *                                   allocation.
     */
    public abstract CS_VirtualMac scheduleVirtualMachine();

    /**
     * Adds a virtual machine to the list of virtual machines managed by this policy.
     *
     * @param vm the {@link CS_VirtualMac} to be added to the list of managed virtual machines.
     *
     * @see #virtualMachines
     */
    public void addVirtualMachine(final CS_VirtualMac vm) {
        this.virtualMachines.add(vm);
    }

    /**
     * Returns the list of virtual machines managed by this policy.
     * <p>
     * <b>Note:</b> This method returns a <i>direct reference</i> to the {@link #virtualMachines}
     * field. Modifying the returned collection in any way will cause unexpected behavior.
     *
     * @return the list of {@link CS_VirtualMac} objects representing the managed virtual machines.
     */
    public List<CS_VirtualMac> getVirtualMachines() {
        return this.virtualMachines;
    }

    /**
     * Returns the list of virtual machines that have had their allocation rejected.
     * <p>
     * <b>Note:</b> This method returns a <i>direct reference</i> to the {@link #rejectedVMs}
     * field. Modifying the returned collection in any way will cause unexpected behavior.
     *
     * @return the list of {@link CS_VirtualMac} objects representing the rejected virtual machines.
     */
    public List<CS_VirtualMac> getRejectedVMs() {
        return this.rejectedVMs;
    }
}
