package ispd.policy.loaders;

import ispd.policy.scheduling.grid.GridSchedulingPolicy;

/**
 * A concrete implementation of the {@link GenericPolicyLoader} class for policies of type
 * {@link GridSchedulingPolicy}.
 * <p>
 * This class overrides the method {@link #getPolicyPackagePath()} to return the path for grid
 * scheduling policies, that being {@value #CLASS_PATH}.
 *
 * @see GenericPolicyLoader
 * @see GridSchedulingPolicy
 */
public class GridSchedulingPolicyLoader extends GenericPolicyLoader<GridSchedulingPolicy> {
    /**
     * The package path of grid scheduling policy implementations.
     */
    private static final String CLASS_PATH = "ispd.policy.scheduling.grid.impl.";

    /**
     * Returns the package path for grid scheduling policy implementations.
     *
     * @return the package path for grid scheduling policies as a string.
     */
    @Override
    protected String getPolicyPackagePath() {
        return GridSchedulingPolicyLoader.CLASS_PATH;
    }
}
