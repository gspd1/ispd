package ispd.policy.loaders;

import ispd.policy.vmallocation.VmAllocationPolicy;

/**
 * A concrete implementation of the {@link GenericPolicyLoader} class for policies of type
 * {@link VmAllocationPolicy}.
 * <p>
 * This class overrides the method {@link #getPolicyPackagePath()} to return the path for VM
 * allocation policies, that being {@value #CLASS_PATH}.
 *
 * @see GenericPolicyLoader
 * @see VmAllocationPolicy
 */
public class VmAllocationPolicyLoader extends GenericPolicyLoader<VmAllocationPolicy> {
    /**
     * The package path of VM allocation policy implementations.
     */
    private static final String CLASS_PATH = "ispd.policy.vmallocation.impl.";

    /**
     * Returns the package path for VM allocation policy implementations.
     *
     * @return the package path for VM allocation policies as a string.
     */
    @Override
    protected String getPolicyPackagePath() {
        return VmAllocationPolicyLoader.CLASS_PATH;
    }
}