package ispd.policy.loaders;

import ispd.arquivo.xml.ConfiguracaoISPD;
import ispd.policy.Policy;
import ispd.policy.PolicyLoader;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An abstract class that provides a method for loading policies. It uses a {@link URLClassLoader}
 * to load classes by their name, and reflection to instantiate them from the acquired {@link Class}
 * objects.
 *
 * <hr>
 * <p>
 * This class may be specialized for each type of policy in the system.
 * <p>
 * Subclasses need to implement the {@link #getPolicyPackagePath()} method to return the package
 * path in which concrete policies of the desired type can be found.
 *
 * @param <T> the type of policy to be loaded; namely, one of
 *            {@link ispd.policy.scheduling.grid.GridSchedulingPolicy GridSchedulingPolicy},
 *            {@link ispd.policy.scheduling.cloud.CloudSchedulingPolicy CloudSchedulingPolicy} or
 *            {@link ispd.policy.vmallocation.VmAllocationPolicy VmAllocationPolicy}.
 */
public abstract class GenericPolicyLoader <T extends Policy<?>> implements PolicyLoader<T> {
    /**
     * A {@link URLClassLoader} used to load {@link Class} objects by name.
     */
    private static final URLClassLoader classLoader = GenericPolicyLoader.makeClassLoader();

    /**
     * Creates and returns a new {@link URLClassLoader} instance using a URL generated from the path
     * of the file {@link ConfiguracaoISPD#DIRETORIO_ISPD}, and its own {@link ClassLoader} as
     * arguments.
     * <p>
     * If a {@link MalformedURLException} is thrown, it is logged with a severity of
     * {@link Level#SEVERE} and an {@link ExceptionInInitializerError}, wrapping the original
     * exception, is thrown.
     *
     * @return the acquired {@link URLClassLoader} instance.
     *
     * @throws ExceptionInInitializerError wrapping a {@link MalformedURLException}, if an error
     *                                     occurs in forming the URL for the loader.
     */
    private static URLClassLoader makeClassLoader() throws ExceptionInInitializerError {
        try {
            return URLClassLoader.newInstance(
                    new URL[] { ConfiguracaoISPD.DIRETORIO_ISPD.toURI().toURL(), },
                    GenericPolicyLoader.class.getClassLoader()
            );
        } catch (final MalformedURLException ex) {
            Logger.getLogger(GenericPolicyLoader.class.getName())
                    .log(Level.SEVERE, "Could not create the loader!", ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * Loads a policy of type {@literal T} with the specified name.
     * <p>
     * The policy is loaded by name using the {@link #classLoader} static field and its default
     * constructor is called to create an instance of the class.
     * <p>
     * If any exceptions are thrown during this process, they are caught, logged iwth a
     * {@link Level#SEVERE} level and a {@link RuntimeException}, wrapping the original excepiton,
     * is thrown.
     *
     * @throws RuntimeException if an error occurs during the policy loading.
     */
    @Override
    public T loadPolicy(final String policyName) {
        final var clsName = this.getPolicyPackagePath() + policyName;

        try {
            final var clazz = GenericPolicyLoader.classLoader.loadClass(clsName);
            return (T) clazz.getConstructor().newInstance();
        } catch (final ClassNotFoundException | InvocationTargetException |
                       InstantiationException | IllegalAccessException |
                       NoSuchMethodException | ClassCastException e) {
            Logger.getLogger(GenericPolicyLoader.class.getName()).log(
                    Level.SEVERE,
                    "Could not load policy '%s'!\n".formatted(policyName),
                    e
            );

            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the package path to be used when loading a policy.
     * <p>
     * This method must be implemented by subclasses of {@code GenericPolicyLoader}.
     *
     * @return the package path for loaded policies.
     */
    protected abstract String getPolicyPackagePath();
}
